package
{
	import core.log.Logger;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Emmanuel LARGE
	 */
	public class Main extends Sprite 
	{
		private var _testModule:TestModule;
		/**
		 * Démarrage du module de test unitaire
		 */
		public function Main() 
		{
			this._testModule = new TestModule ();
			this.addChild (this._testModule);
			var test:MyMathArrondiParamTest = new MyMathArrondiParamTest ();
			this._testModule.addToTestList (test);
			this._testModule.runAllTest ();
		}
		
		
	}
	
}