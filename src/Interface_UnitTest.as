package 
{
	import flash.events.IEventDispatcher;
	
	/**
	 * ...
	 * @author Emmanuel LARGE
	 */
	public interface Interface_UnitTest extends IEventDispatcher
	{
		function get result ():Boolean;
		function get testName ():String;
		function run ():void;
	}
	
}