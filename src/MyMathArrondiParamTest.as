package 
{
	import core.log.Logger;
	import core.math.MyMath;
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * ...
	 * @author Emmanuel LARGE
	 */
	public class MyMathArrondiParamTest extends Sprite implements Interface_UnitTest
	{
		private var _testData:Array;
		private var _result:Boolean;
		private var _testName:String;
		private var _logger:Logger;
		
		public function MyMathArrondiParamTest() 
		{
			super();
			this._logger = Logger.instance;
			this._testName = "MyMathArrondiParamTest"
			this.initTest ();
		}
		
		/**
		 * initialisation des jeux de test
		 */
		private function initTest():void 
		{
			//idéalement devrais lever une erreur mais bizzarerie du language
			this._testData = new Array ();
			this._testData[0] = new Array ();
			this._testData[0][0] = new Array ();
			this._testData[0][0][0] = new String (" test erreur entrée 1");
			this._testData[0][0][1] = new int (1);
			this._testData[0][0][2] = new Number (1);
			this._testData[0][1] = NaN;
			this._testData[0][2] = null;
			
			//idéalement devrais lever une erreur mais bizzarerie du language n°2
			this._testData[1] = new Array ();
			this._testData[1][0] = new Array ();
			this._testData[1][0][0] = new Object ();
			this._testData[1][0][0].test = new Array();
			this._testData[1][0][0].alpha = new int (0);
			this._testData[1][0][1] = new int (1);
			this._testData[1][0][2] = new Number (1);
			this._testData[1][1] = NaN;
			this._testData[1][2] = null;
			
			//idéalement devrais lever une erreur mais bizzarerie du language n°3 (j'y vois comme un pattern)
			this._testData[2] = new Array ();
			this._testData[2][0] = new Array ();
			this._testData[2][0][0] = new Number (3.141592653589793238462643383279502884197169399375105820974944);
			this._testData[2][0][1] = new int (0);
			this._testData[2][0][2] = new Number (0);
			this._testData[2][1] = Infinity;
			this._testData[2][2] = null;
			
			// test simple a l'unité 
			this._testData[3] = new Array ();
			this._testData[3][0] = new Array ();
			this._testData[3][0][0] = new Number (3.141592653589793238462643383279502884197169399375105820974944);
			this._testData[3][0][1] = new int (0);
			this._testData[3][0][2] = new Number (1);
			this._testData[3][1] = new Number (3);
			this._testData[3][2] = null;
			
			// test simple a la 5 em decimal
			this._testData[4] = new Array ();
			this._testData[4][0] = new Array ();
			this._testData[4][0][0] = new Number (3.141592653589793238462643383279502884197169399375105820974944);
			this._testData[4][0][1] = new int (5);
			this._testData[4][0][2] = new Number (1);
			this._testData[4][1] = new Number (3.14159);
			this._testData[4][2] = null;
			
			// test simple a la 5 em decimal ave un pas de 5 
			this._testData[5] = new Array ();
			this._testData[5][0] = new Array ();
			this._testData[5][0][0] = new Number (3.141592653589793238462643383279502884197169399375105820974944);
			this._testData[5][0][1] = new int (5);
			this._testData[5][0][2] = new Number (5);
			this._testData[5][1] = new Number (3.14160);
			this._testData[5][2] = null;
			
			// test simple a la 10 em decimal ave un pas de 0.5 
			this._testData[6] = new Array ();
			this._testData[6][0] = new Array ();
			this._testData[6][0][0] = new Number (3.141592653589793238462643383279502884197169399375105820974944);
			this._testData[6][0][1] = new int (10);
			this._testData[6][0][2] = new Number (0.5);
			this._testData[6][1] = new Number (3.1415926536);
			this._testData[6][2] = null;
			
			// test simple a la 60em decimal avec un pas de 1  ( non arrondis donc )
			this._testData[7] = new Array ();
			this._testData[7][0] = new Array ();
			this._testData[7][0][0] = new Number (3.141592653589793238462643383279502884197169399375105820974944);
			this._testData[7][0][1] = new int (60);
			this._testData[7][0][2] = new Number (1);
			this._testData[7][1] = new Number (3.141592653589793238462643383279502884197169399375105820974944);
			this._testData[7][2] = null;
			
		}
		
		/**
		 * execution de tout les sous test
		 */
		public function run ():void
		{
			this._result = false;
			var success:Array = new Array ();
			//itération sur les subtest
			for (var i:int = 0; i < this._testData.length; i++)
			{
				success[i] = new Boolean (false);
				
				//chargement d'un jeu de test
				var data:Array = this._testData[i][0] as Array;
				var predictedResult:* = this._testData[i][1];
				var predictedError:Error = this._testData[i][2];
			
				try
				{
					//appel a la fonction tester
					var res:* = MyMath.arrondiParam (data[0], data[1], data[2]); 
				}
				catch (err:Error)
				{
					//cas des erreurs
					if ( err === predictedError)
					{
						success[i] = true;
					}
				}
				
				//cas bizzarerie 1 et 2
				if  ( isNaN (res) && isNaN(predictedResult) )
				{
					success[i] = true;
				}
				
				//cas bizzarerie 3
				if  ( !isFinite(res) && !isFinite(predictedResult) )
				{
					success[i] = true;
				}
				
				//cas normal (comparaison du resultat predictif et réel)
				if  ( res == predictedResult )
				{
					success[i] = true;
				}
				
				//info utilisateur
				this._logger.log ("subtest "+i+" of " + this._testName+", Result is : " + success[i]);
			}
			
			// check global
			if ( success.lastIndexOf (false) == -1)
			{
				this._result = true;
			}
			
			//fin du test
			this.dispatchEvent (new Event (Event.COMPLETE));
		}
		
		/**
		 * 
		 */
		public function get result():Boolean 
		{
			return _result;
		}
		
		/**
		 * 
		 */
		public function get testName():String 
		{
			return _testName;
		}
	}

}