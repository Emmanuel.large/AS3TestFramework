package 
{
	import core.log.Logger;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.InterfaceAddress;
	
	/**
	 * ...
	 * @author Emmanuel LARGE
	 */
	public class TestModule extends Sprite 
	{
		private var _logger:Logger;
		private var _testVector:Vector.<Interface_UnitTest>;
		
		public function TestModule() 
		{
			super();
			this._logger = Logger.instance;
			this._logger.setLogState (Logger.LOG_STATE_FULL);
			this._logger.addCC (this);		
			this._testVector = new Vector.<Interface_UnitTest>;
		}
		
		/**
		 * Ajout d'un test a la liste de test globale
		 * @param	aTest
		 */
		public function addToTestList (aTest:Interface_UnitTest):void
		{
			this._testVector.push (aTest);
		}
		
		/**
		 * Execution d'un Test et affichage du résultat
		 * @param	aTest
		 */
		public function runOneTest (aTest:Interface_UnitTest):void
		{
			this.uiAddResult (aTest);
			aTest.run ();
		}
		
		/**
		 * Execution de l'ensemble des test
		 *
		 */
		public function runAllTest ():void
		{
			for each ( var test:Interface_UnitTest in this._testVector)
			{
				this.uiAddResult (test);
				test.run ();
			}
		}
		
		/**
		 *  Affichage des résultats des test dans l'interface du module
		 * 
		 * 	faute de temps pour l'instant un log physique (fichier log/date.log et log/date-error.log)
		 * 	et un trace dans le debuggeur sont effectué via le logger
		 * 
		 * @param	test
		 */
		private function uiAddResult(test:Interface_UnitTest):void 
		{
			//TO DO Real UI 
			test.addEventListener (Event.COMPLETE,onResult);
		}
		
		/**
		 * Fin d'un test, affichage du résultat
		 * @param	e
		 */
		private function onResult(e:Event):void 
		{
			var test:Interface_UnitTest = e.currentTarget as Interface_UnitTest;
			this._logger.log ("result of " + test.testName + " : " + test.result);
		}
		
	}

}